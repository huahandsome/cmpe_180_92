----	input 	----

1. type for input-file stream: 	ifstream in_stream;
   type for output-file stream:	ofstream out_stream;
   
2. header:	
	#include <fstream>
	
3. associate stream variable to a file:
	in_stream.open("filename");
	
4. read two input numbers from files:
	int one_number, another_number;
	in_stream >> one_number >> another_number;

	

	
----	output	----
1. when used with a stream of type ofstream, the member function open will CREATE
	the output file if it does not already exist.
	
2.if the output file does already EXIST, the member function open will discard the contents of
  the file so that the output file is empty after the call to open.
  
3. the open() function in ofstream is overloadded, which mean you also can append the content to the file, not
	cause the file to be clean.
	
	out_stream.open( "filename", ios::app );
	
	- if the file does not exist, this will create an empty file with that name;
	- if the file already exists, then all the output from your program will be appended to the end of the file.
	
	
----	check whether the file is open successfully		----
1. there is a fail() member function for each of the classes ifstream & ofstream.

2. prototype of fail():	bool fail();	- without argument;

3. You should place a call to fail immediately after each call to open.

	in_stream.open("filename");
	if ( in_stream.fail() ) {
		cout << "Error msg" << endl;
		exit(1);
	}
	

	
----	File name as Input		----
	
	char file_name[16];
	ifstream in_stream;
	
	cout << "enter file name :" << endl;
	cin >> file_name;
	
	in_stream.open(file_name);
	if ( in_stream.fail() ) {
		cout << "Error msg" << endl;
		exit(1);
	}
	
	
----	SUMMARY		----
Just copy from Page318

*** Place the following include directives in your program file:

	#include <fstream>		<---- For file I/O
	#include <iostream>		<---- For cout
	#include <cstdlib>		<---- For exit
	

***	Choose a stream name for the input stream (for example, in_stream), and declare it to be 
	a variable of type ifstream. 
	Choose a stream name for the output stream (for example, out_stream), and declare it to be 
	of type ofstream.
	
	using namespace std;
	ifstream in_stream;
	ofstream out_stream;
	
*** Connect each stream to a file using the member function open with the external file name as 
	an argument. Remember to use the member function fail to test that the call to open was successful:
	
	in_stream.open("filename");
	if ( in_stream.fail() ){
		cout << "Error msg" << endl;
		exit(1);
	}
	
	out_stream.open("filename1");
	if ( out_stream.fail() ) {
		cout << "Error msg" << endl;
		exit(1);
	}
	
*** Using the strema in_stream to get input from the file infile.dat just like you use cin
	to get input from the keyboard. For example:
	
	in_stream >> some_variable >> some_other_variable;
	
*** Use the stream out_stream to send output to the file outfile.dat just like you use cout to 
	send output to the screen. For example:
	
	out_stream << "Some msg " << some_variable << endl;

*** Close the streams using the function close:
	
	in_stream.close();
	out_stream.close();
	
*** Magic format

	cout.setf(ios::FLAG);	<---- member function setf()
	cout.width();
		OR
	setw();
	setprecision()			<---- non-member function setw() and setprecision();
	
*** Checking at the end of the file

*** Member function get & put
	
	Every input stream has a member function named get that can be used 
	to read one character of input:
	
	Every output stream has a member function named put, which takes one argument
	which should be an expression of type char.
	
	char next_symbol;                    char put_symbol = 'a';
	cin.get(next_symbol);				 cout.put(put_symbol);
	
	
	
	cin.get( next );
    while( next != ' ' ) {
		cout.put( next );
        cin.get( next );
    }
	
	直到输入回车键，输入的字符流才会进入 stream，才会进行下一步处理。(while(next != ' ' ) )
	
	cin.get( next ); --> 等待字符流有东西，然后取一个字符。 那么在输入的时候，以回车表明用户输入结束，字符流有东西。
	
	
*** check input

		#include <cstdlib>
		#include <iostream>

		using namespace std;

		void new_line() {
			char symbol;
			do {
				cin.get(symbol);
			} while( symbol != '\n');
		}

		void get_int ( int& number ){
			char ans;
			do {
				cout << "Enter number: ";
				cin >> number;
				cout << "correct?:";
				cin >> ans;    // for debug
				cout << ans;
				new_line();    // it is necessary
			} while( ans != 'y' && ans != 'Y' );
		}

		int main(int argc, char** argv)
		{
			int n;
			
			get_int(n);
			
			cout << "Final value" << n << endl;
			return 0;
		}
		
		1) 分析 new_line()的必要性。
		
		
*** The eof Member Function
	Every input-file stream has a member function called eof that can be used to determine
	when all of the file has been read and there is no more input left for the program.
	
	