/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Administrator
 *
 * Created on January 26, 2017, 10:37 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstring>
#include <vector>

using namespace std;

/* ****************
 * CONSTANT 
 * ****************/
const char* R_FILENAME = "presidents.in.txt";
const unsigned int INDENT_FIR_NAME  = strlen( "First name" )  + 2;
const unsigned int INDENT_MID_NAME = strlen( "Middle name ") + 2;
const unsigned int INDENT_LST_NAME = strlen( "Last name" ) + 2;
const unsigned int INDENT_BORN = strlen ( "Born" ) + 1;
const unsigned int INDENT_DIED = strlen ( "Died" ) + 2;
const unsigned int INDENT_AGE = strlen( "Age" );
const unsigned int INDENT_START = strlen("Start") + 1;
const unsigned int INDENT_END = strlen("End") + 2;
const unsigned int INDENT_PARTY = strlen("Party");

/* ******************
 *  GLOBAL VAR 
 * ******************/
unsigned int g_num_of_pre_died = 0;

/* *******************************
 *  FUNCTION PROTOTYPE 
 * *******************************/  
/** 
 * @description: read the name of president from file 
 * 
 * @para:
 *      input:  file-stream for read a file
 *      first[out]  : first name of president
 *      mid[out]  : middle name of president
 *      last[out]  : last name of president
 * 
 * @return: void
 */
void r_name(  ifstream& input, string& first, string& mid, string& last );

/** 
 * @description: write out full name to standart output 
 * 
 * @para:
 *      first[in]  : first name of president
 *      mid[in]  : middle name of president
 *      last[in]  : last name of president
 * 
 * @return: void
 */
void w_name( string first, string mid, string last);

/** 
 * @description: read the year of born & died from file 
 * 
 * @para:
 *      input[in]   : file-stream for read a file
 *      born[out]  : born year of the president
 *      died[out]  : died year of president
 * 
 * @return: void
 */
void r_age( ifstream& input, unsigned int& born, unsigned int& died ) ;

/** 
 * @description: write out year of born/die to standard output 
 * 
 * @para:
 *      born[out]  : born year of president
 *      died[out]  : died year of president
 * 
 * @return: void
 */
void w_age( unsigned int born, unsigned int died );

/** 
 * @description: read the name of party from file 
 * 
 * @para:
 *      input[in]   : file-stream for read a file
 *      born[out]  : get party of the president
 * 
 * @return: void
 */
void r_party( ifstream& input, string& party ) ;

/** 
 * @description: write out party to standart output 
 * 
 * @para:
 *      party[in]  : born year of president
 * 
 * @return: void
 */
void w_party( const string& party );

/** 
 * @description: read the year of work from file 
 * 
 * @para:
 *      input[in]   : file-stream for read a file
 *      start[out]  : get year of join a party of the president
 *      end[out]   : get year of leave a party of the president
 * 
 * @return: void
 */
void r_yearOfwork( ifstream& input, unsigned int& start, unsigned int& end );

/** 
 * @description: write out year of work to standart output 
 * 
 * @para:
 *      startin]  : year of join a party
 *      end[in] : year of leave a party
 *  
 * @return: void
 */
void w_yearOfwork( unsigned int start, unsigned int end );

/** 
 * @description: cal the total age of died presidents 
 * 
 * @para:
 *      age[in]  : age of each pass president
 *  
 * @return: double, average age of presidents
 */
double cal_total_age( unsigned int age );

/** 
 * @description: write out the average age to standard output 
 * 
 * @para: void
 *  
 * @return: void
 */
void w_avg_age( );

/** 
 * @description: write out header format to standard output 
 * 
 * @para: void
 *  
 * @return: void
 */
void format_header( );

/** 
 * @description: determine the end of file 
 * 
 * @para: 
 *      input[in]   : file-stream that read a file
 *  
 * @return: true if meet the period(.); else return false
 */
bool end_of_file ( ifstream& input );

/** 
 * @description: read the carrier return of each line from 
 *                          the file and write out to standard output 
 * 
 * @para: 
 *      input[in]   : file-stream that read a file
 *  
 * @return: void
 */
void rw_line_end ( ifstream& input );

/*
 * 
 */
int main(int argc, char** argv)
{
    ifstream input;
    unsigned int start, end, born, died;
    string first, middle, last, party;
    
    input.open( R_FILENAME );
    
    if ( input.fail() ) {
        cout << "Error to open file: " << R_FILENAME << endl;
        exit(1);
    }
    else {    
        format_header( ) ;
        
        while ( end_of_file ( input ) == false ) {
            r_name( input, first, middle, last );
            w_name( first, middle, last );
        
            r_age( input, born, died ) ;
            w_age( born, died );
        
            r_party( input, party ) ;        
            r_yearOfwork( input, start, end );
        
            w_yearOfwork( start, end );
            w_party( party );
            
            rw_line_end( input );
        }
        
        w_avg_age( );
    }
    
    input.close();
    
    return 0;
}

void r_name( ifstream& input, string& first, string& middle, string& last ) {
    string name;
    vector<string> vec_name;
    
    input >> name; 
    while ( name != "(" ) {
        vec_name.push_back(name);
        input >> name;
    }
    
    if ( 2 == vec_name.size() ) {
        first = vec_name.at(0);
        middle = " ";
        last = vec_name.at(1);
    }
    else {
        first = vec_name.at(0);
        middle = vec_name.at(1);
        last = vec_name.at(2);
    }
    
    vec_name.clear();   //clear vector
}

void w_name( string first, string middle, string last ) {
    cout << left << setw( INDENT_FIR_NAME ) << first;
    cout << left << setw( INDENT_MID_NAME ) << middle;
    cout << left << setw( INDENT_LST_NAME ) << last;
}

void r_age( ifstream& input, unsigned int& born, unsigned int& died ) {
    char c;
    died = 9999;    // default value
    input >> born;  // read year of born
    
    // ignore space & try to read year of died
    input >> c; 
    while ( c != ')' ) {
        if ( c == '-' ) {
            input >> died;  // read year of died
            cal_total_age( died - born ); // cal total age
            g_num_of_pre_died++;
        }
        input >> c;
    }
}

void w_age( unsigned int born, unsigned int died ) {
    if ( died != 9999 ) {
        cout << left << setw( INDENT_BORN ) << born;
        cout << left << setw( INDENT_DIED ) << died;
        cout << right << setw( INDENT_AGE ) << died - born;
    }
    else {
        cout << left << setw( INDENT_BORN ) << born;
        cout << left << setw( INDENT_DIED ) << " ";
        cout << left << setw( INDENT_AGE ) << " ";
    }
}

void r_party( ifstream& input, string& party ) {
    input >> party;
}    

void w_party( const string& party ) {
    cout << left << setw(2) << "  ";
    cout << left << setw( INDENT_PARTY ) << party;
}

void r_yearOfwork( ifstream& input, unsigned int& start, unsigned int& end ) {
    char c;
    input >> start; // read year of start work
    
    input.get( c ) ;
    if ( c != ' ' ) {
        input.putback(c);
        end = start;
    }
    else {
        input >> c;  // read hyphen
        input >> end;   // read year of end work
    }
}

void w_yearOfwork( unsigned int start, unsigned int end ) {
    cout << right << setw( INDENT_START ) << start;
    cout << right << setw( INDENT_END ) << end;
}

void rw_line_end( ifstream& input ) {
    char c;
    input.get( c ); // read //r
    cout << c;
    input.get( c ); // read //n
    cout << c;
}

double cal_total_age( unsigned int age ) {
    static double total_age = 0.0;
    total_age += age;
    
    return total_age;
}

bool end_of_file ( ifstream& input ) {
    char c;
    bool res = true;
    
    input.get(c);
    
    if ( c != '.' ) {
        input.putback(c);
        res = false;
    }
    
    return res;
}

void w_avg_age( ) {
    cout << endl;
    cout << setprecision(3) << "Average age at death = " << cal_total_age(0)/g_num_of_pre_died;
}

void format_header( ) {
    // first line
    cout << left << setw( INDENT_FIR_NAME ) << "First name";
    
    cout << left << setw( INDENT_MID_NAME ) << "Middle name";
    
    cout << left << setw( INDENT_LST_NAME ) << "Last name";
    
    cout << left << setw( INDENT_BORN ) << "Born";
    
    cout << left << setw( INDENT_DIED ) << "Died";
    
    cout << left << setw( INDENT_AGE ) << "Age";
    
    cout << right << setw( INDENT_START ) << "Start";
    
    cout << right << setw( INDENT_END ) << "End";
    
    cout << left << setw(2) << "  ";
    
    cout << left << setw( INDENT_PARTY ) << "Party" << endl;
    
    // 2nd line
    cout << left << setw( INDENT_FIR_NAME ) << "----------";
    
    cout << left << setw( INDENT_MID_NAME ) << "-----------";
    
    cout << left << setw( INDENT_LST_NAME ) << "---------";
    
    cout << left << setw( INDENT_BORN ) << "----";
    
    cout << left << setw( INDENT_DIED ) << "----";
    
    cout << left << setw( INDENT_AGE ) << "---";
    
    cout << right << setw( INDENT_START ) << "-----";
    
    cout << right << setw( INDENT_END ) << "----";
 
    cout << left << setw(2) << "  ";
    
    cout << left << setw( INDENT_PARTY ) << "-----" << endl;
}