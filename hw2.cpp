/*
 * cmpe_hw2.cpp
 *
 *  Created on: Feb 2, 2017
 *      Author: carl
 */

#include <iostream>
#include <cstring>
#include <ctime>
#include <cstdlib>

using namespace std;

const string U_WIN = "user win";
const string P_WIN = "pc win";
const string TIE   = "tie";

int get_user_input();
int get_pc_input();
void get_statistic( string res, int& u_win, int& p_win, int& tie );
void put_statistic(int u_win, int p_win, int tie );
void put_error_msg();
string get_result( int user_input, int pc_input);

int main() {
	int u_input, p_input;
	string res;
	int u_count = 0, p_count = 0, t_count = 0;
	srand( time(0) );
	while ( ( u_input = get_user_input() ) != 4 ) {
		if ( u_input != 0 ) {
			p_input = get_pc_input();

			res = get_result( u_input, p_input );

			get_statistic( res, u_count, p_count, t_count );

			cout << "The Result is:" << res << endl ;
			cout << "-----------------------------" << endl << endl;
		}
		else {
			put_error_msg();
		}
	}

	put_statistic( u_count, p_count, t_count );

	return 0;
}

int get_user_input() {
	string input;
	int res;

	cout << "Please select a choice: (1, 2, 3, 4)" << endl;
	cout << "1) Rock " << endl;
	cout << "2) Paper" << endl;
	cout << "3) Scissor" << endl;
	cout << "4) Exit" << endl;

	cin >> input;

	if ( input == "1" ) {
		res = 1;
	}
	else if ( input == "2" ) {
		res = 2;
	}
	else if ( input == "3" ) {
		res = 3;
	}
	else if ( input == "4" ) {
		res = 4;
	}
	else {
		res = 0;
	}

	return res;
}

int get_pc_input(){
	// It is important to seed the random number generator only once.
	//srand( time(0) );
	int num = rand()%3 + 1;

	// for debug
	cout << "pc input: " << num << endl;

	return num;
}

string get_result( int user_input, int pc_input) {
	string res;

	if ( user_input == pc_input ) {
		res = TIE;
		return res;
	}

	if ( user_input == 1 ) {
		if ( pc_input == 2 ) {
			res = P_WIN;
		}
		else if ( pc_input == 3 ) {
			res = U_WIN;
		}
	}
	else if ( user_input == 2 ) {
		if ( pc_input == 1 ){
			res = U_WIN;
		}
		else if ( pc_input == 3 ){
			res = P_WIN;
		}
	}
	else if ( user_input == 3 ) {
		if ( pc_input == 1 ) {
			res = P_WIN;
		}else if ( pc_input == 2 ){
			res = U_WIN;
		}
	}

	return res;
}


void get_statistic( string res, int& u_win, int& p_win, int& tie ) {
	if ( res == TIE ) {
		tie++;
	}
	else if ( res == P_WIN ){
		p_win++;
	}
	else if ( res == U_WIN ) {
		u_win++;
	}
}

void put_statistic(int u_win, int p_win, int tie ) {
	cout << "User win: " << u_win << endl;
	cout << "PC win: " << p_win << endl;
	cout << "Tie: " << tie << endl;
}

void put_error_msg() {
	cout << "Please provide correct input" << endl;
}
